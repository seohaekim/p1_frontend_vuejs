import Vue from 'vue'
import Vuex from 'vuex'
import ES6Promise from 'es6-promise'

import Constant from '@/utils/Constant'

ES6Promise.polyfill()
Vue.use(Vuex)

const State = {
  ColorList: {
    Base: ['green', 'darken-2'],
    CardTitle: ['green', 'darken-2'],
    NotImportantCardTitle: ['indigo'],
    BaseBg: ['green', 'darken-2'],
    BaseBg2: ['white'],
    BaseBtn: ['green', 'darken-2', 'white--text'],
    CurrentBtn: ['success', 'white--text'],
    DeleteBtn: ['error'],
    CancelBtn: ['orange', 'darken-2'],
    None: ['grey', 'lighten-2']
  },
  CompanyImgData: {
    url: {
      small: null,
      medium: null,
      default: null
    },
    idx: null,
    imgKind: null,
    imgOrgName: null,
    uploadedPath: null
  },
  meData: {
    isAuth: false,
    userType: null,
    userPhone: null,
    userName: null
  },

  App: {
    BoardIdx: null
  }
}

const Mutations = {
  [Constant.GET_COMPANY_IMG_DATA]: (state, payload) => {
    state.CompanyImgData.idx = payload.idx
    state.CompanyImgData.imgKind = payload.imgKind
    state.CompanyImgData.imgOrgName = payload.imgOrgName
    state.CompanyImgData.uploadedPath = payload.uploadedPath

    let FileType = payload.url.slice(payload.url.lastIndexOf('.'))

    state.CompanyImgData.url.default = payload.url
    state.CompanyImgData.url.small = payload.url.replace(FileType, `-small${FileType}`)
    state.CompanyImgData.url.medium = payload.url.replace(FileType, `-medium${FileType}`)
  },
  [Constant.GET_ME_DATA]: (state, payload) => {
    if (payload.userPhone && payload.userName && payload.userType) state.meData.isAuth = true

    state.meData.userType = payload.userType
    state.meData.userPhone = payload.userPhone
    state.meData.userName = payload.userName
  },

  [Constant.APP_BOARD_CLICK]: (state, payload) => {
    state.App.BoardIdx = (state.App.BoardIdx === payload.idx) ? null : payload.idx
  }
}

const Getters = {
  getBaseColor (state) {
    let MatchCall = ''

    state.ColorList.CardTitle.forEach(el => {
      MatchCall = `${el} ${MatchCall}`
    })

    state.ColorList['MatchCall'] = MatchCall.trim()

    return state.ColorList
  },
  getCompanyData (state) {
    return state.CompanyImgData
  },
  getMeData (state) {
    return state.meData
  },

  getAppBoardIdx (state) {
    return state.App
  }
}

const Store = () => {
  return new Vuex.Store({
    state: State,
    mutations: Mutations,
    getters: Getters
  })
}

export default Store
