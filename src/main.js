// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import 'event-source-polyfill'

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import '@/configs/AxiosConfig'
import * as VueGoogleMaps from 'vue2-google-maps'
import Vue2Filters from 'vue2-filters'
import '@/utils/filters'
import VueMoment from 'vue-moment'
import ko from '@/configs/locale/ko'
import VeeValidate from 'vee-validate'
import VueProgressBar from 'vue-progressbar'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'vue-datetime/dist/vue-datetime.css'
import Datetime from 'vue-datetime'

import ImsPlugin from './plugin/Ims-Plugin'

require('es6-promise').polyfill()

Vue.config.productionTip = false
Vue.prototype.$eBus = new Vue()

Vue.use(Datetime)

/**
 * Main components and theme
 * ref: https://vuetifyjs.com/
 */
Vue.use(Vuetify)

/**
 * vue validator
 * ref: https://github.com/baianat/vee-validate
 */
Vue.use(VeeValidate, {
  locale: 'ko',
  dictionary: ko
})

/**
 * vue moment with moment filter
 * ref: https://github.com/brockpetrie/vue-moment
 */
Vue.use(VueMoment)

/**
 * vue2 standard filters
 * ref: https://github.com/freearhey/vue2-filters
 */
Vue.use(Vue2Filters)

/**
 * google map setting
 */
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCYwtY8QVdWYLtY5mXClS6lM_VBD791FOg',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    // If you want to set the version, you can do so:
    // v: '3.26',
  }

  // If you intend to programmatically custom event listener code
  // (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  // instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  // you might need to turn this on.
  // autobindAllEvents: false,

  // If you want to manually install components, e.g.
  // import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  // Vue.component('GmapMarker', GmapMarker)
  // then disable the following:
  // installComponents: true,
})

/**
 * vue-progressbar
 */
Vue.use(VueProgressBar, {
  color: 'yellow',
  failedColor: 'red',
  thickness: '6px',
  autoRevert: true
})

Vue.use(ImsPlugin)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
