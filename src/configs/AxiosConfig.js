import Vue from 'vue'
import axios from 'axios'

let host = process.env.IMS_HOST

const imsAPI = axios.create({
  baseURL: host,
  timeout: 10000,
  crossDomain: true,
  withCredentials: true
  // headers: {'X-Custom-Header': 'CustomHeader1'}
})

// another api service
const amazonAPI = axios.create({
  baseURL: 'https://amazon-domain.com/api/',
  timeout: 3000,
  headers: {'X-Custom-Header': 'CustomHeader2'}
})

Vue.prototype.$ims = imsAPI

export default {
  imsAPI,
  amazonAPI,
  host
}
