const dictionary = {
  ko: {
    messages: {
      required: '필수입력 항목입니다',
      numeric: '숫자형식만 입력 가능합니다',
      regex: '올바른 형식이 아닙니다',
      email: '올바른 이메일형식이 아닙니다',

      min: function (field, len) {
        return `최소 ${len} 글자 이상 입력하여야 합니다`
      },

      max: function (field, len) {
        return `최대 ${len} 글자까지 입력 가능합니다`
      }
    }
  }
}

export default dictionary
