import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import Home from '@/components/home'

Vue.use(Router)

const requireAuth = () => (from, to, next) => {
  if (localStorage.getItem('isAuth') === 'true') return next()

  next('/login')
}

const isLoginedMoveHome = () => (from, to, next) => {
  if (localStorage.getItem('isAuth') === 'true') return next('/home')

  next()
}


export default new Router({
  /* Vue.js 리우트 히스토리 모드 */
  mode: 'history',
  /* ---------------------- */
  routes: [
    /* 로그인 및 회원가입, 회원정보 찾기 */
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: isLoginedMoveHome()
    },
    /* 메인 */
    {
      path: '/', redirect: '/home'
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      beforeEnter: requireAuth()
    }
  ]
})
