export default {
  GET_COMPANY_IMG_DATA: 'GetCompanyImgData',
  GET_ME_DATA: 'GetMeData',

  APP_BOARD_CLICK: 'AppBoardClick'
}
