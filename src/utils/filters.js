/**
 * 공통 필터처리
 * @author mk
 * @since 2018. 07
 */

import Vue from 'vue'

Vue.filter('scheduleDate', function (scheduleDate) {
  if (scheduleDate !== null && scheduleDate !== undefined) {
    let FirstscheduleDate = scheduleDate.substr(0, 10)
    let SecondcheduleDate = scheduleDate.substr(22, 10)

    return `${FirstscheduleDate} ~ ${SecondcheduleDate}`
  }
})
Vue.filter('phoneNo', function (phoneNo) {
  if (phoneNo !== null && phoneNo !== undefined) {
    if (phoneNo.indexOf('02') === 0) return phoneNo.replace(/(\d{2})(\d{3,4})(\d{4})/, '$1-$2-$3')
    else if (phoneNo.indexOf('15') === 0) return phoneNo.replace(/(\d{4})(\d{4})/, '$1-$2')
    else return phoneNo.replace(/(\d{3})(\d{3,4})(\d{4})/, '$1-$2-$3')
  }
})

Vue.filter('birthDay', function (birthDay) {
  if (birthDay) {
    return birthDay.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3')
  }
})

Vue.filter('carAutomaticYn', function (carAutomaticYn) {
  switch (carAutomaticYn) {
    case 'Y':
      return '오토'
    case 'N':
      return '수동'
  }
})

Vue.filter('carStatus', function (carStatus) {
  switch (carStatus) {
    case 'Y':
      return '정상'
    case 'S':
      return '매각'
    case 'D':
      return '폐차'
  }
})

Vue.filter('Gender', function (custGender) {
  switch (custGender) {
    case 'F':
      return '여자'
    case 'M':
      return '남자'
  }
})

Vue.filter('limitDate', function (limitDate) {
  return limitDate.substr(0, 10)
})


Vue.prototype.$filters = Vue.options.filters
