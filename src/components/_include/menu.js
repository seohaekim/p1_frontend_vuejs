/**
 * Menu
 * @author mk
 */

const items = [
  /*
  회원가입 - completed (디자인 완료)
  비밀번호 찾기 - completed (디자인 완료)
  */
  { icon: 'home', text: 'Home', to: '/home' }, // 업체등록 notice - completed (디자인 완료)
  {
    icon: 'business',
    model: false,
    text: '회사관리',
    children: [
      { icon: 'info', text: '회사정보', to: '/company/update' }, // completed (디자인 완료)
      { icon: 'room', text: '지점현황', to: '/company/branch?page=1' }, // completed (디자인 완료)
      { icon: 'supervisor_account', text: '직원현황', to: '/company/empl?page=1' } // completed (디자인 완료)
    ]
  },
  {
    icon: 'directions_car',
    model: false,
    text: '차량관리',
    children: [
      { icon: 'directions_car', text: '차량현황', to: '/car?page=1' }, // complpeted (디자인 완료)
      { icon: 'view_list', text: '스케쥴 현황 - 일별', to: '/schedule/day' }, // completed (디자인 완료)
      // { icon: 'access_time', text: '스케쥴 현황 - 시간별', to: '/car/schedule/time' }, // hidden
      { icon: 'build', text: '정비현황', to: '/maint?page=1' } // completed (디자인 완료)
    ]
  },
  {
    icon: 'airport_shuttle',
    text: '대여관리',
    model: false,
    children: [
      { icon: 'sms', text: '예약현황', to: '/matchCall' }, // completed (디자인 완료)
      { icon: 'event_note', text: '주문현황', to: '/order?page=1' } // completed (디자인 완료)
    ]
  },
  {
    icon: 'face',
    text: '고객관리',
    children: [
      { icon: 'person', text: '개인고객', to: '/customer/pers?page=1' }, // completed (디자인 완료)
      { icon: 'work', text: '법인고객', to: '/customer/corp?page=1' } // completed (디자인 완료)
    ]
  },
  { icon: 'contacts', text: '거래처관리', to: '/partner?page=1' }, // completed (디자인 완료)
  { icon: 'forum', text: '커뮤니티', to: '/community?page=1' },
  { icon: 'work', text: '양수양도', to: '/transfer?page=1' },
  { icon: 'notification_important', text: '공지사항', to: '/notice?page=1' }
]

export default items
