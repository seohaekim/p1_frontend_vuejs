import { mapGetters } from 'vuex'
import moment from 'moment'

import DashBoardContent from './DashBoardContent'

export default {
  name: 'home',

  components: { DashBoardContent },

  data: function () {
    return {
      gradient: '',
      DefaultSearchParams: {
        limit: 5,
        offset: 0,
      },
      noticeList: [],
      communityList: [],
      transfers: []
    }
  },
  computed: {
    ...mapGetters(['getBaseColor'])
  },
  methods: {
    moment () { return moment },
    async getNoticeList () {
      let self = this
      let sendData = {}

      sendData.limit = self.DefaultSearchParams.limit
      sendData.offset = self.DefaultSearchParams.offset

      try {
        let res = await this.$ims.get('/v1/items/dashboard')

        self.noticeList = res.data.items.notice
        self.communityList = res.data.items.communities
        self.transfers = res.data.items.transfers
      } catch (e) {
        console.log(e)
      }
    }
  },
  mounted: function () {
    this.getNoticeList()
    // this.getImg()
  }
}

