export default {
  install (Vue) {
    Vue.prototype.$InputMask = (type, InputString, _this) => {
      if (type === 'number' && InputString) {
        _this = InputNumber.GetResetNumber(InputString)
      } else if (type === '###' && InputString) {
        _this = InputCount.GetResetCount(InputString)
      }

      if (_this === 'null') _this = null

      return _this
    }
  }
}

let InputNumber = {
  str: '',

  ResetNumber: function (v) {
    this.str = v.split('-').join('')
  },
  GetResetNumber: function (v) {
    this.ResetNumber(v)

    if (this.str.indexOf('0') > -1) {
      if (this.str.indexOf('02') === 0) {
        this.str = this.str.replace(/(\d{2})(\d{3,4})(\d{4})/, '$1-$2-$3')
      } else {
        this.str = this.str.replace(/(\d{3})(\d{3,4})(\d{4})/, '$1-$2-$3')
      }
    } else {
      this.str = this.str.replace(/(\d{3,4})(\d{4})/, '$1-$2')
    }

    return this.str
  }
}

let InputCount = {
  str: '',
  outStr: '',

  ResetCount: function (v) {
    this.str = v.split(',').join('')
  },
  GetResetCount: function (v) {
    this.ResetCount(v)
    this.SetCount()

    return this.outStr
  },
  SetCount: function () {
    let ReStr

    if (this.str.indexOf('0') === 0) this.str = this.str.replace('0', '')

    if (this.str.length < 4) ReStr = this.str
    else if (this.str.length < 7) ReStr = this.str.replace(/(\d{1,3})(\d{3})/, '$1,$2')
    else if (this.str.length < 10) ReStr = this.str.replace(/(\d{1,3})(\d{3})(\d{3})/, '$1,$2,$3')
    else if (this.str.length < 13) ReStr = this.str.replace(/(\d{1,3})(\d{3})(\d{3})(\d{3})/, '$1,$2,$3,$4')
    else if (this.str.length < 16) ReStr = this.str.replace(/(\d{1,3})(\d{3})(\d{3})(\d{3})(\d{3})/, '$1,$2,$3,$4,$5')
    else if (this.str.length < 19) ReStr = this.str.replace(/(\d{1,3})(\d{3})(\d{3})(\d{3})(\d{3})(\d{3})/, '$1,$2,$3,$4,$5,$6')
    else if (this.str.length < 21) ReStr = this.str.replace(/(\d{1,3})(\d{3})(\d{3})(\d{3})(\d{3})(\d{3})(\d{3})/, '$1,$2,$3,$4,$5,$6,$7')

    this.outStr = ReStr
  }
}
